task("update_token_name", "Update token name")
    .addParam("symbol", "The token symbol")
    .addParam("name", "The token name")
    .setAction(async function (args, hre, runSuper) {
        const accounts = await ethers.getSigners();
        const sender = accounts[0].address;
        console.log("Sender address: ", sender);

        const network = hre.network.name;

        const fs = require('fs');
        const dotenv = require('dotenv');
        const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`))
        for (const k in envConfig) {
            process.env[k] = envConfig[k]
        }

        const bridge = await hre.ethers.getContractAt("DecimalBridge", process.env.BRIDGE_ADDRESS);
        console.log("Bridge address:", bridge.address);
        await bridge.updateTokenName(args.symbol, args.name);
        console.log("Success!");
    });
