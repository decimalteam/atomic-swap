task("create_swap_decimal", "Create swap to Decimal chain")
    .setAction(async function (args, hre, runSuper) {
        const Web3 = require('web3');
        const web3 = new Web3(hre.network.provider);
        const accounts = await ethers.getSigners();
        const sender = accounts[0].address;

        console.log("Sender address: ", sender);

        const network = hre.network.name;

        const fs = require('fs');
        const dotenv = require('dotenv');
        const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`))
        for (const k in envConfig) {
            process.env[k] = envConfig[k]
        }

        const bridge = await hre.ethers.getContractAt("DecimalBridge", process.env.BRIDGE_ADDRESS);
        console.log("Bridge address:", bridge.address);

        console.log("Creating swap...");
        await bridge.swapToDecimal(
            process.env.SWAP_AMOUNT,
            process.env.SWAP_NONCE,
            process.env.SWAP_RECIPIENT,
            process.env.SWAP_SYMBOL
        )

        message = web3.utils.soliditySha3(
            { t: 'uint256', v: process.env.SWAP_NONCE },
            { t: 'uint256', v: process.env.SWAP_AMOUNT },
            { t: 'string', v: process.env.SWAP_SYMBOL },
            { t: 'string', v: process.env.SWAP_RECIPIENT },
            { t: 'uint256', v: process.env.SWAP_FROM },
            { t: 'uint256', v: process.env.SWAP_TO }
        );

        console.log("Message:", await bridge.swaps(message));
    });
