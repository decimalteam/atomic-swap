task("add_admin", "Add a new admin to the bridge")
    .addParam("user", "The users address")
    .setAction(async function (args, hre, runSuper) {
        const accounts = await ethers.getSigners();
        const sender = accounts[0].address;

        console.log("Sender address: ", sender);

        const network = hre.network.name;

        const fs = require('fs');
        const dotenv = require('dotenv');
        const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`))
        for (const k in envConfig) {
            process.env[k] = envConfig[k]
        }

        const bridge = await hre.ethers.getContractAt("DecimalBridge", process.env.BRIDGE_ADDRESS);
        console.log("Bridge address:", bridge.address);
        
        let admin_role = await bridge.ADMIN_ROLE();

        console.log("Try to add a new admin to the bridge...");

        await bridge.grantRole(admin_role, args.user);
        console.log("Is admin:", await bridge.hasRole(admin_role, args.user));
    });