task("update_chain", "Add an allowed chain id to the bridge")
    .addParam("chain", "The chain id")
    .addParam("enabled", "Enabled bool")
    .setAction(async function (args, hre, runSuper) {
        const accounts = await ethers.getSigners();
        const sender = accounts[0].address;

        console.log("Sender address: ", sender);

        const network = hre.network.name;

        const fs = require('fs');
        const dotenv = require('dotenv');
        const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`))
        for (const k in envConfig) {
            process.env[k] = envConfig[k]
        }

        const bridge = await hre.ethers.getContractAt("DecimalBridge", process.env.BRIDGE_ADDRESS);
        console.log("Bridge address:", bridge.address);
        
        console.log("Try to update chain in bridge...");
        await bridge.updateChain(args.chain, args.enabled);
        console.log("Done.");
    });