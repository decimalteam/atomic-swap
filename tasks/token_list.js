task("token_list", "View list of tokens")
    .setAction(async function (args, hre, runSuper) {
        const accounts = await ethers.getSigners();
        const sender = accounts[0].address;
        console.log("Sender address: ", sender);

        const network = hre.network.name;

        const fs = require('fs');
        const dotenv = require('dotenv');
        const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`))
        for (const k in envConfig) {
            process.env[k] = envConfig[k]
        }

        const bridge = await hre.ethers.getContractAt("DecimalBridge", process.env.BRIDGE_ADDRESS);
        const DERC20 = await hre.ethers.getContractFactory("DERC20");
        console.log("Bridge address:", bridge.address);

        let token_list = await bridge.getTokenList();

        for (i=0; i<token_list.length; i++ ){
            let token = await DERC20.attach(token_list[i]);
            console.log(await token.name(), await token.symbol(), await token.decimals(), token.address);
        }
    });