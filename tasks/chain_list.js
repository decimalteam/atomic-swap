task("chain_list", "Chain list")
    .setAction(async function (args, hre, runSuper) {
        const accounts = await ethers.getSigners();
        const sender = accounts[0].address;
        console.log("Sender address: ", sender);

        const network = hre.network.name;

        const fs = require('fs');
        const dotenv = require('dotenv');
        const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`))
        for (const k in envConfig) {
            process.env[k] = envConfig[k]
        }

        const bridge = await hre.ethers.getContractAt("DecimalBridge", process.env.BRIDGE_ADDRESS);
        console.log("Bridge address:", bridge.address);
        
        console.log("Chain list in", network, ":");
        console.log("DEL:", await bridge.chainList(1));
        console.log("ETH:", await bridge.chainList(2));
        console.log("BSC:", await bridge.chainList(3));
        console.log("Done.");
    });