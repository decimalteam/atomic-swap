task("add_validator", "Add a new validator to the bridge")
    .addParam("validator", "The validator address")
    .setAction(async function (args, hre, runSuper) {
        const accounts = await ethers.getSigners();
        const sender = accounts[0].address;

        console.log("Sender address: ", sender);

        const network = hre.network.name;

        const fs = require('fs');
        const dotenv = require('dotenv');
        const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`))
        for (const k in envConfig) {
            process.env[k] = envConfig[k]
        }

        const bridge = await hre.ethers.getContractAt("DecimalBridge", process.env.BRIDGE_ADDRESS);
        console.log("Bridge address:", bridge.address);
        
        validator_role = await bridge.VALIDATOR_ROLE();

        console.log("Try to add a new validator to the bridge...");

        await bridge.grantRole(validator_role, args.validator);
        console.log("Is validator:", await bridge.hasRole(validator_role, args.validator));
    });
