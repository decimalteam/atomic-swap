// We require the Hardhat Runtime Environment explicitly here. This is optional 
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");
const network = hre.network.name;
const fs = require('fs');
const dotenv = require('dotenv');
const { default: BigNumber } = require('bignumber.js');
BigNumber.config({ EXPONENTIAL_AT: 60 });
const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`))
for (const k in envConfig) {
  process.env[k] = envConfig[k]
}

async function main() {
  const accounts = await ethers.getSigners();
  const sender = accounts[0].address;
  console.log("Sender address: ", sender);

  const Bridge = await hre.ethers.getContractFactory("DecimalBridge");
  console.log("Deploying...");
  const bridge = await Bridge.deploy(process.env.CHAIN_ID);
  await bridge.updateChain(1, true);
  await bridge.updateChain(2, true);
  await bridge.updateChain(3, true);

  console.log("Bridge has been deployed to:", bridge.address);
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
