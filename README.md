# Decimal Bridge
Decimal Bridge is decentralized application based on smart contracts structure. User can change coins between different blockchains(DEL, ETH, BSC).

Installation
===
```bash 
npm install -D
```

Compile project
===
```bash
npx hardhat compile
```

Execute tests
===
```bash
npx hardhat --network hardhat test
```

Deploy contract
===
```bash
npx hardhat run scripts/deploy_bridge.js --network {network_name}
```
Argument __network_name__ may be _bsctestnet_, _bscmainnet_, _mainnet_, _goerli_, _kovan_, _rinkeby_, _ropsten_.  

Configuration
===
## Add an allowed chain id to the bridge
```bash
npx hardhat update_chain --chain {chain_id} --enabled true --network {network_name}
```
where __chain_id__ is _1_ - for Decimal chain, _2_ - Ethereum, _3_ - Binance Smart Chain.  

## Add a validator address to a bridge
```bash
npx hardhat add_validator --validator {validator_address} --network {network_name}
```
where __validator_address__ - address of a validator.  

## Add a new admin to a bridge
```bash
npx hardhat add_admin --user {user_address} --network {network_name}
```
where __user_address__ - address of a new admin.  

## Manual addition token to the bridge
```bash
npx hardhat add_token --symbol {symbol} --network {network_name}
```
where __symbol__ - symbol of a token.  

## Update name of an existing token
```bash
npx hardhat update_token_name --symbol {symbol} --name {token_name} --network {network_name}
```
where __symbol__ - symbol of an existing token,  
__token_name__ - name of token.  


Other
===
## View list of tokens
```bash
npx hardhat token_list --network {network_name}
```

## Create swap to Decimal chain
To create swap you should configure .env file and add parameters:  
*SWAP_AMOUNT* - amount of tokens  
*SWAP_NONCE* - swap number  
*SWAP_RECIPIENT* - recipient address  
*SWAP_TO* - destination chain id  
*SWAP_SYMBOL* - symbol of token  
```bash
npx hardhat create_swap_decimal --network {network_name}
```

## Create swap
To create swap you should configure .env file and add parameters:  
*SWAP_AMOUNT* - amount of tokens  
*SWAP_NONCE* - swap number  
*SWAP_RECIPIENT* - recipient address  
*SWAP_TO* - destination chain id  
*SWAP_SYMBOL* - symbol of token  
```bash
npx hardhat create_swap --network {network_name}
```

## Execute redeem
To execute redeem you should configure .env file and add parameters:  
*REDEEM_VALIDATOR* - redeem validator key, validator address must have validator role in bridge  
*SWAP_AMOUNT* - amount of tokens  
*SWAP_NONCE* - swap number  
*SWAP_RECIPIENT* - recipient address  
*SWAP_TO* - destination chain id  
*SWAP_SYMBOL* - symbol of token  
```bash
npx hardhat execute_redeem --network {network_name}
```