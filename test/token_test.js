const { expect } = require("chai");
const { ethers } = require("hardhat");
//const { parseUnits } = require("ethers/utils");

const BigNumber = require('bignumber.js');
BigNumber.config({ EXPONENTIAL_AT: 60 });

const Web3 = require('web3');
const web3 = new Web3(hre.network.provider);


const token_name = "Decimal coin"
const token_symbol = "tDEL"
const amount = "100000000000000000000"; //parseUnits("100");
let bridge;
let token;
let bridge_owner;
let recipient;
let new_admin;
let new_minter;
let new_burner;
let default_admin_role;
let admin_role;
let minter_role;
let burner_role;
let pauser_role;

describe("Admin token via bridge", () => {
    beforeEach(async () => {
        [bridge_owner, recipient, new_admin, new_minter, new_burner] = await ethers.getSigners();
        const Bridge = await ethers.getContractFactory("DecimalBridge");
        const DERC20 = await ethers.getContractFactory("DERC20");
        bridge = await Bridge.deploy(1, { "gasLimit": 5000000 });
        await bridge.deployed();
        await bridge.addToken(token_symbol);
        token_addr = await bridge.tokenBySymbol(token_symbol);
        token = await DERC20.attach(token_addr);
        other_token = await DERC20.deploy(token_symbol);
        await other_token.deployed();
        default_admin_role = await token.DEFAULT_ADMIN_ROLE();
        admin_role = await token.ADMIN_ROLE();
        minter_role = await token.MINTER_ROLE();
        burner_role = await token.BURNER_ROLE();
        pauser_role = await token.PAUSER_ROLE();
    });
    it('STEP 1: Bridge address must have DEFAULT_ADMIN_ROLE, ADMIN_ROLE, MINTER_ROLE, BURNER_ROLE roles', async () => {
        expect(
            await token.hasRole(default_admin_role, bridge.address)
        ).to.equal(true);
        expect(
            await token.hasRole(admin_role, bridge.address)
        ).to.equal(true);
        expect(
            await token.hasRole(minter_role, bridge.address)
        ).to.equal(true);
        expect(
            await token.hasRole(burner_role, bridge.address)
        ).to.equal(true);
    });

    it('STEP 2: Add token without admin role: fail', async () => {
        try {
            await bridge.connect(new_admin).addToken(token_symbol);
        } catch (error) {
            expect(error.message).to.include("DecimalBridge: Caller is not an admin");
        }
    });

    it('STEP 3: Update token without admin role: fail', async () => {
        try {
            await bridge.connect(new_admin).updateToken(token_symbol, other_token.address);
        } catch (error) {
            expect(error.message).to.include("DecimalBridge: Caller is not an admin");
        }
    });
    it('STEP 4: Update token should be update address of token', async () => {
        expect(
            await bridge.tokenBySymbol(token_symbol)
        ).to.equal(token.address);
        await bridge.updateToken(token_symbol, other_token.address);
        expect(
            await bridge.tokenBySymbol(token_symbol)
        ).to.equal(other_token.address);
    });
    it('STEP 3: Mint token without minter role: fail', async () => {
        try {
            await bridge.mintToken(token_symbol, recipient.address, amount);
        } catch (error) {
            expect(error.message).to.include("DecimalBridge: Caller is not a minter");
        }
    });
    it('STEP 4: Mint token: success', async () => {
        expect(
            await token.balanceOf(recipient.address)
        ).to.equal(0);
        await bridge.grantRole(minter_role, bridge_owner.address);
        await bridge.mintToken(token_symbol, recipient.address, amount);
        expect(
            await token.balanceOf(recipient.address)
        ).to.equal(amount);
    });
    it('STEP 5: Burn token without burner role: fail', async () => {
        try {
            await bridge.burnToken(token_symbol, recipient.address, amount);
        } catch (error) {
            expect(error.message).to.include("DecimalBridge: Caller is not a burner");
        }
    });
    it('STEP 6: Burn token: success', async () => {
        await bridge.grantRole(minter_role, bridge_owner.address);
        await bridge.grantRole(burner_role, bridge_owner.address);
        await bridge.mintToken(token_symbol, recipient.address, amount);
        expect(
            await token.balanceOf(recipient.address)
        ).to.equal(amount);
        await bridge.burnToken(token_symbol, recipient.address, amount);
        expect(
            await token.balanceOf(recipient.address)
        ).to.equal(0);
    });
    it('STEP 7: Grant role token without admin role :fail', async () => {
        try {
            await bridge.connect(new_admin).grantRoleToken(token_symbol, admin_role, new_admin.address);
        } catch (error) {
            expect(error.message).to.include("DecimalBridge: Caller is not an admin");
        }
    });
    it('STEP 8: Grant role token', async () => {
        expect(
            await token.hasRole(admin_role, new_admin.address)
        ).to.equal(false);
        expect(
            await token.hasRole(minter_role, new_minter.address)
        ).to.equal(false);
        expect(
            await token.hasRole(burner_role, new_burner.address)
        ).to.equal(false);

        await bridge.grantRoleToken(token_symbol, admin_role, new_admin.address);
        await bridge.grantRoleToken(token_symbol, minter_role, new_minter.address);
        await bridge.grantRoleToken(token_symbol, burner_role, new_burner.address);

        expect(
            await token.hasRole(admin_role, new_admin.address)
        ).to.equal(true);
        expect(
            await token.hasRole(minter_role, new_minter.address)
        ).to.equal(true);
        expect(
            await token.hasRole(burner_role, new_burner.address)
        ).to.equal(true);

    });
    it('STEP 9: Update chain', async () => {
        expect(
            await bridge.chainList(1)
        ).to.equal(false);
        await bridge.updateChain(1, true);
        expect(
            await bridge.chainList(1)
        ).to.equal(true);
    });
    it('STEP 10: Update token name', async () => {
        expect(
            await token.name()
        ).to.equal("");
        await bridge.updateTokenName(token_symbol, token_name);
        expect(
            await token.name()
        ).to.equal(token_name);
    });
    it('STEP 11: Pause token without pauser role: fail', async () => {
        try {
            await bridge.pauseToken(token_symbol);
        } catch (error) {
            expect(error.message).to.include("DecimalBridge: Caller is not a pauser");
        }
    });
    it('STEP 12: Pause token: success', async () => {
        await bridge.grantRole(pauser_role, bridge_owner.address);
        await bridge.grantRoleToken(token_symbol, pauser_role, bridge.address);
        expect(
            await token.paused()
        ).to.equal(false);
        await bridge.pauseToken(token_symbol);
        expect(
            await token.paused()
        ).to.equal(true);
    });
    it('STEP 13: Unpause token without pauser role: fail', async () => {
        try {
            await bridge.unpauseToken(token_symbol);
        } catch (error) {
            expect(error.message).to.include("DecimalBridge: Caller is not a pauser");
        }
    });
    it('STEP 14: Unpause token: success', async () => {
        await bridge.grantRole(pauser_role, bridge_owner.address);
        await bridge.grantRoleToken(token_symbol, pauser_role, bridge.address);
        await bridge.pauseToken(token_symbol);
        expect(
            await token.paused()
        ).to.equal(true);
        await bridge.unpauseToken(token_symbol);
        expect(
            await token.paused()
        ).to.equal(false);
    });
});