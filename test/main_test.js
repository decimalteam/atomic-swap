const { expect } = require("chai");
const { ethers } = require("hardhat");
const BigNumber = require('bignumber.js');
BigNumber.config({ EXPONENTIAL_AT: 60 });
const Web3 = require('web3');
const web3 = new Web3(hre.network.provider);

const transaction_number = 1;
const token_symbol = "DEL"
const chainBSC = 3;
const chainETH = 2;
const amount = "100000000000000000000";


let bridge_owner;
let validator;
let not_validator;
let sender;
let recipient;

describe("Main bridge functions", () => {
  let bridge;
  let del_token;
  beforeEach(async () => {
    [bridge_owner, sender, recipient, validator, not_validator] = await ethers.getSigners();
    const Bridge = await ethers.getContractFactory("DecimalBridge");
    const DERC20 = await ethers.getContractFactory("DERC20");
    bridge = await Bridge.deploy(chainETH);
    await bridge.deployed();
    await bridge.addToken(token_symbol);
    await bridge.grantRole(await bridge.VALIDATOR_ROLE(), validator.address);
    await bridge.updateChain(1, true);
    await bridge.updateChain(3, true);
    let token_addr = await bridge.tokenBySymbol(token_symbol);
    del_token = await DERC20.attach(token_addr);
  });

  describe('Bridge: deploy', () => {
    it('STEP 1: Deployer address must have ADMIN_ROLE role', async () => {
      expect(
        await bridge.hasRole(await bridge.ADMIN_ROLE(), bridge_owner.address)
      ).to.equal(true);
    });
    it('STEP 2: Validator address must have VALIDATOR_ROLE role', async () => {
      expect(
        await bridge.hasRole(await bridge.VALIDATOR_ROLE(), validator.address)
      ).to.equal(true);
    });
  });

  describe('bridge: swap', () => {
    beforeEach(async () => {

      let recipient_addr = sender.address;
      message = web3.utils.soliditySha3(
        { t: 'uint256', v: transaction_number },
        { t: 'uint256', v: amount },
        { t: 'string', v: token_symbol },
        { t: 'address', v: recipient_addr },
        { t: 'uint256', v: chainBSC },
        { t: 'uint256', v: chainETH }
      );

      let signature = await web3.eth.sign(message, validator.address);
      let sig = ethers.utils.splitSignature(signature)

      await bridge.connect(sender).redeem(
        amount,
        recipient_addr,
        transaction_number,
        chainBSC,
        sig.v,
        sig.r,
        sig.s,
        token_symbol,
        { gasLimit: 6721975 }
      );
    });
    it('STEP 1: Swap with same chain id: fail', async () => {
      try {
        await bridge.connect(sender).swap(amount, transaction_number, recipient.address, chainETH, token_symbol);
        throw new Error("Not reverted");
      } catch (error) {
        expect(error.message).to.include("DecimalBridge: Invalid chainTo id");
      }
    });
    it('STEP 2: Swap with not empty state: fail', async () => {
      await bridge.connect(sender).swap(amount, transaction_number, recipient.address, chainBSC, token_symbol);

      try {
        await bridge.connect(sender).swap(amount, transaction_number, recipient.address, chainBSC, token_symbol);
        throw new Error("Not reverted");
      } catch (error) {
        expect(error.message).to.include("DecimalBridge: Swap is not empty state or duplicate tx");
      }
    });
    it('STEP 4: Swap not deployed token: fail', async () => {
      try {
        await bridge.connect(sender).swap(amount, transaction_number, recipient.address, chainBSC, "dul");
        throw new Error("Not reverted");
      } catch (error) {
        expect(error.message).to.include("DecimalBridge: Token is not registered");
      }
    });

    it('STEP 5: Success swap:', async () => {
      expect(
        await del_token.balanceOf(sender.address)
      ).to.be.equal(amount);

      let recipient_addr = recipient.address;

      await bridge.connect(sender).swap(amount, transaction_number, recipient.address, chainBSC, token_symbol);

      message = web3.utils.soliditySha3(
        { t: 'uint', v: transaction_number },
        { t: 'uint', v: amount },
        { t: 'string', v: token_symbol },
        { t: 'address', v: recipient_addr },
        { t: 'uint256', v: chainETH },
        { t: 'uint256', v: chainBSC }
      );

      let data = await bridge.swaps(message);
      expect(data.transaction).to.equal(transaction_number);
      expect(data.state).to.equal(1);
      expect(
        await del_token.balanceOf(sender.address)
      ).to.be.equal(0);
    });
  });

  describe('Bridge: redeem', () => {
    let recipient_addr;
    let message;
    beforeEach(async () => {
      recipient_addr = recipient.address;
      message = web3.utils.soliditySha3(
        { t: 'uint256', v: transaction_number },
        { t: 'uint256', v: amount },
        { t: 'string', v: token_symbol },
        { t: 'address', v: recipient_addr },
        { t: 'uint256', v: chainBSC },
        { t: 'uint256', v: chainETH }
      );
    });

    it('STEP 1: Redeem with invalid chain id: fail', async () => {
      let signature = await web3.eth.sign(message, validator.address);
      let sig = ethers.utils.splitSignature(signature)

      try {
        await bridge.connect(sender).redeem(
          amount,
          recipient_addr,
          transaction_number,
          chainETH,
          sig.v,
          sig.r,
          sig.s,
          token_symbol,
        );
        throw new Error("Not reverted");
      } catch (error) {
        expect(error.message).to.include("DecimalBridge: Invalid chainFrom id");
      }
    });

    it('STEP 2: Should revert if swap is not in an Empty state', async () => {

      let signature = await web3.eth.sign(message, validator.address);
      let sig = ethers.utils.splitSignature(signature)

      await bridge.connect(sender).redeem(
        amount,
        recipient_addr,
        transaction_number,
        chainBSC,
        sig.v,
        sig.r,
        sig.s,
        token_symbol,
      );

      try {
        await bridge.connect(sender).redeem(
          amount,
          recipient_addr,
          transaction_number,
          chainBSC,
          sig.v,
          sig.r,
          sig.s,
          token_symbol,
        );
        throw new Error("Not reverted");
      } catch (error) {
        expect(error.message).to.include("DecimalBridge: Swap is not empty state or duplicate tx");
      }
    });

    it('STEP 3: Should revert if the provided message was not signed by the validator', async () => {
      let signature = await web3.eth.sign(message, not_validator.address);
      let sig = ethers.utils.splitSignature(signature);
      try {
        await bridge.connect(sender).redeem(
          amount,
          recipient_addr,
          transaction_number,
          chainBSC,
          sig.v,
          sig.r,
          sig.s,
          token_symbol
        );
        throw new Error("Not reverted");
      } catch (error) {
        expect(error.message).to.include("DecimalBridge: Validator address is invalid");
      }
    });
    it('STEP 4: Redeem: success', async () => {
      expect(
        await del_token.balanceOf(recipient.address)
      ).to.be.equal(0);
      let signature = await web3.eth.sign(message, validator.address);
      let sig = ethers.utils.splitSignature(signature)

      await bridge.connect(sender).redeem(
        amount,
        recipient_addr,
        transaction_number,
        chainBSC,
        sig.v,
        sig.r,
        sig.s,
        token_symbol,
      );

      expect(
        await del_token.balanceOf(recipient.address)
      ).to.be.equal(amount);

    });
  });
});